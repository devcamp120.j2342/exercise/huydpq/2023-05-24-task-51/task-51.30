import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Stack;

public class App {
    public static void main(String[] args) throws Exception {
        // task 1: Cho một chuỗi str, xoá các ký tự xuất hiện nhiều hơn một lần trong
        // chuỗi và chỉ giữ lại ký tự đầu tiên
        String str1 = "bananas";
        Set<Character> set = new LinkedHashSet<>();
        for (char c : str1.toCharArray()) {
            set.add(c);
        }
        StringBuilder sb = new StringBuilder();
        for (Character c : set) {
            sb.append(c);
        }
        System.out.println("Task 1: " + sb);

        // task 2: Cho một xâu kí tự, đếm số lượng các từ trong xâu kí tự đó ( các từ có
        // thể cách nhau bằng nhiều khoảng trắng )
        String str2 = "Abc  abc bac";
        String[] words = str2.trim().split("\\s+");
        System.out.println("Task 2: " + words.length);

        // task 3: Cho hai xâu kí tự s1, s2 nối xâu kí tự s2 vào sau xâu s1
        String str3 = "abc", str3_1 = "xyz";

        System.out.println("Task 3: " + str3 + str3_1);

        // task 4: Cho hai xâu kí tự s1, s2, kiểm tra xem chuỗi s1 chứa chuỗi s2 không?
        String str4 = "Devcamp java", str4_1 = "java";
        Boolean task4 = str4.contains(str4_1);
        System.out.println("Task 4: " + task4);

        // task 5: Hiển thị ký tự thứ 3 trong string
        System.out.println("Task 5: " + str4.charAt(2));
        // task 6: đếm số lần xuất hiện của một ký tự trong một xâu
        int count = 0;
        char a6 = 'a';
        for (int i = 0; i < str4.length(); i++) {
            if (str4.charAt(i) == a6) {
                count++;
            }
        }
        System.out.println("Task 6: Số lần xuất hiện ký tự a trong chuỗi là:  " + count);

        // task 7: Tìm vị trí xuất hiện lần đầu tiên của một ký tự trong một xâu
        String str7 = "Devcamp java";
        char a7 = 'a';
        int c = str7.indexOf(a6);
        System.out.println("Task 7: " + c);

        // task 8: Chuyển các ký tự in thường sang in hoa

        String str8 = "Devcamp";
        System.out.println("Task 8: " + str8.toUpperCase());

        // task 9: Chuyển các ký tự in thường sang in hoa và ngược lại
        String str9 = "DevcAMP";
        StringBuilder sb9 = new StringBuilder();
        for (int i = 0; i < str9.length(); i++) {
            char c9 = str9.charAt(i);
            if (Character.isUpperCase(c9)) {
                sb9.append(Character.toLowerCase(c9));
            } else {
                sb9.append(Character.toUpperCase(c9));
            }
        }
        System.out.println("Task 9: " + sb9.toString());

        // task 10: Đếm số ký tự in hoa trong một xâu
        String str10 = "DevcAMP123";
        int count10 = 0;
        for (int i = 0; i < str10.length(); i++) {
            char c10 = str10.charAt(i);
            if (Character.isUpperCase(c10)) {
                sb9.append(Character.toLowerCase(c10));
                count10++;
            }
        }
        System.out.println("Task 10: " + count10);

        // task 11: Hiển thị ra màn hình các ký tự từ A tới Z
        String str11 = "DevcAMP123";
        StringBuilder sb11 = new StringBuilder();
        for (int i = 0; i < str11.length(); i++) {
            char c11 = str10.charAt(i);
            if (c11 >= 'A' && c11 <= 'Z') {
                sb11.append(c11);

            }
        }
        System.out.println("Task 11: " + sb11);

        // task12: Cho một chuỗi str và số nguyên n >= 0. Chia chuỗi str ra làm các phần
        // bằng nhau với n ký tự. Nếu chuỗi không chia được thì xuất ra màn hình “KO”.
        String str12 = "abcdefghijklmnopqrstuvwxy";
        int n12 = 5;
        System.out.println("Task 12: " + Arrays.toString(task12(str12, n12)));
        ;

        // task 13: Xóa các phần tử liền kề giống nhua
        String str13 = "aabaarbarccrabmq";
        task13(str13);

        // task 14: Cho 2 string, gắn chúng lại với nhau, nếu 2 chuỗi có độ dài không
        // bằng nhau thì tiến hành cắt bỏ các ký tự đầu của string dài hơn cho đến khi
        // chúng bằng nhau thì tiến hành gắn lại
        String str14 = "Welcome";
        String str14_1 = "home";
        task14(str14, str14_1);

        // task 15: Chuyển các ký tự in thường sang in hoa và ngược lại
        String str15 = "DevCamp";
        StringBuilder sb15 = new StringBuilder();
        for (int i = 0; i < str15.length(); i++) {
            char a15 = str15.charAt(i);
            if (Character.isUpperCase(a15)) {
                sb15.append(Character.toLowerCase(a15));
            } else {
                sb15.append(Character.toUpperCase(a15));
            }
        }
        System.out.println("Task 15: " + sb15);

        // task 16: Kiểm tra xem một chuỗi có xuất hiện số hay không
        String str16 = "Devcamp12";
        System.out.println("Task 16: " + task16(str16));

        // task 17: Kiểm tra chuỗi này có phù hợp với yêu cầu hay không
        // Yêu cầu về chuỗi là: Có độ dài không quá 20 ký tự, không được chứa ký tự
        // khoảng trắng, bắt đầu bằng một ký tự chữ viết hoa (A - Z), kết thúc bằng một
        // số (0 - 9). (Sử dụng biểu thức chính quy để ràng buộc định dạng)
        String str17 = "DevCamp123";

        String regex = "^[A-Za-z]{0,20}[0-9]+$";
        System.out.println(str17.matches(regex));

    }

    // task 12:
    public static String[] task12(String str, int n) {
        if (n <= 0 || str.length() % n != 0) {
            return new String[] { "KO" };
        }
        int numParts = str.length() / n;

        String[] parts = new String[numParts];
        for (int i = 0; i < numParts; i++) {
            int indexStart = i * n;
            int indexEnd = (i + 1) * n;
            parts[i] = str.substring(indexStart, indexEnd);
        }
        return parts;
    }

    // task 13
    public static void task13(String str) {
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < str.length(); i++) {
            char currentChar = str.charAt(i);

            // Kiểm tra xem ngăn xếp có rỗng không và ký tự hiện tại giống ký tự đầu tiên
            // trong ngăn xếp
            if (!stack.isEmpty() && currentChar == stack.peek()) {
                stack.pop(); // Xoá ký tự trong ngăn xếp nếu giống nhau
            } else {
                stack.push(currentChar); // Thêm ký tự vào ngăn xếp nếu khác nhau
            }
        }
        StringBuilder result = new StringBuilder();

        // Xây dựng chuỗi kết quả từ ngăn xếp
        while (!stack.isEmpty()) {
            result.insert(0, stack.pop()); // Thêm ký tự vào đầu chuỗi kết quả
        }
        System.out.println("Task 13: " + result);
    }

    // task 14:
    public static void task14(String str1, String str2) {
        int len1 = str1.length();
        int len2 = str2.length();

        if (len1 > len2) {
            str1 = str1.substring(len1 - len2);
        } else if (len1 < len2) {
            str2 = str2.substring(len2 - len1);
        }

        System.out.println("Task 14: " + str1 + str2);

    }

    // task 16
    public static Boolean task16(String str) {
        for (int i = 0; i < str.length(); i++) {
            char a = str.charAt(i);
            if (Character.isDigit(a)) {
                return true;
            }
        }
        return false;
    }

}
